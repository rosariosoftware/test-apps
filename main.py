import pandas as pd  # This is always assumed but is included here as an introduction.
import numpy as np
import sys

if (sys.version_info[0] != 3) or (sys.version_info[1] != 7):
    raise Exception("You must use python 3.7 for this script to work.")

np.random.seed(0)

values = np.random.randn(100) # array of normally distributed random numbers
s = pd.Series(values) # generate a pandas series
print(str(s))