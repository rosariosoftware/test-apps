if (process.version !== "v14.17.3") {
    throw new Error("Error, node version must be v14.17.3 for this to work.")
} else {
    console.log('Hello from NodeJS')
}
